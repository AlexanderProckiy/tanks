let temp = 1;
class Tank {
    constructor(ammunition) {
        console.dir('Создан танк №' + temp++);
        this.ammunition = ammunition;
    }

    moveTo(x, y) {

    }

    fireTo(x, y) {
        if (this.canFire()) {
            this.ammunition = this.ammunition - 1;
        } else {console.log('Снаряды закончились')}
    }
    moveAndFire(moveX, moveY, fireX, fireY) {
        this.moveTo(moveX, moveY);
        this.fireTo(fireX, fireY);
    }
    canFire() {
        if (this.ammunition > 0) {
            return true
        } else {
            return false
        }
    }
}

const tank1 = new Tank(10);
tank1.moveTo(10, 10);

const tank2 = new Tank(5);
tank2.moveTo(20, 20);

console.log(tank2.ammunition);
tank2.fireTo(10, 10);
console.log(tank2.ammunition);
